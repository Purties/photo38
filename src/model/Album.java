package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javafx.scene.image.Image;

/**
 * Album object class that contains its fields which include album name, list of photos and reference to oldest photo and
 * latest photo. It implements Serializable interface so that it can't be store as binary file and loaded it into programming
 * as needed.
 * @author Xiaofa Lin Yiwen Tao
 *
 */
public class Album implements Serializable {
	/**
	 * Album name filed.
	 */
	private String name;
	/**
	 * List of photos in an album.
	 */
	private List<Photo> photos;
	/**
	 * reference to Photo object which indicate the oldest one in an album .
	 */
	private Photo oldestPhoto;
	/**
	 * reference to Photo object which indicate the latest one in an album.
	 */
	private Photo earliestPhoto;

	/**
	 * Album constructor which initializes all the fields.
	 * @param name - A required parameter for Album, need a name to distinct different albums.
	 */
	public Album(String name) 
	{
		this.name = name;
		oldestPhoto = null;
		earliestPhoto = null;
		photos = new ArrayList<Photo>();
	}

	/**
	 * A setters to change(rename) albums' name.
	 * @param name - Take new name as parameter and replace old one with it.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * A getter to get an albums' name
	 * @return album name - return selected album name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * A method allows to add photos into a photo list which contained in an album
	 * @param photo - a new photo to be added.
	 */
	public void addPhoto(Photo photo) {
		photos.add(photo);
		findOldestPhoto();
		findEarliestPhoto();
	}

	/**
	 * A method allows to remove photo from a photo list(an album) by giving its index in the list.
	 * @param index - a index of photo that would be removed.
	 */
	public void removePhoto(int index) {
		photos.remove(index);
		findOldestPhoto();
		findEarliestPhoto();
	}

	/**
	 * A method to get photo object from photo list by receiving a index as parameters.
	 * @param index		index at which the photo belongs in the list of photos
	 * @return photo at index in photos list
	 */
	public Photo getPhoto(int index) {
		return photos.get(index);
	}

	/**
	 * A method to get the size of photo list which means get the number of photos in the list.
	 * @return How many photos in the list(album).
	 */

	public int getCount() {
		return photos.size();
	}


	/**
	 * A method to get Image object from photo list.
	 * @return image of first photo in the list
	 */

	public Image getAlbumPhoto() {
		if (photos.isEmpty()) {
			return null;
		}
		return photos.get(0).getImage();
	}

	/**
	 * A method to get list of photos.
	 * @return List of Photos
	 */
	public List<Photo> getPhotos() {
		return photos;
	}

	/**
	 * A method to find oldest photo in an album.
	 */
	public void findOldestPhoto() {
		if (photos.size() == 0) {
			return;
		}
		Photo temp = photos.get(0);

		for (Photo p : photos) {
			if (p.getCalendar().compareTo(temp.getCalendar()) < 0) {
				temp = p;
			}
		}
		oldestPhoto = temp;
	}


	/**
	 * A method to find latest photo in an album.
	 */
	public void findEarliestPhoto() {
		if (photos.size() == 0) {
			return;
		}
		Photo temp = photos.get(0);
		for (Photo p : photos) {
			if (p.getCalendar().compareTo(temp.getCalendar()) > 0) {
				temp = p;
			}
		}
		earliestPhoto = temp;
	}

	/**
	 * A method to get string representation of oldest photo's date.
	 * @return oldest photo date
	 */
	public String getOldestPhotoDate() {
		if (oldestPhoto == null) {
			return "N/A";
		}
		return oldestPhoto.getDate();
	}

	/**
	 * A method to get string representation of oldest photo's date.
	 * @return earliest photo date
	 */
	public String getEarliestPhotoDate() {
		if (earliestPhoto == null)
			return "N/A";
		return earliestPhoto.getDate();
	}

	/**
	 * A method get required string representation of date range from oldest to latest.
	 * @return date of oldest to latest photo date
	 */
	public String getDateRange() {
		return getOldestPhotoDate() + " - " + getEarliestPhotoDate();
	}

	/**
	 * A method that returns an index number by checking match photo object with giving the photo object.
	 * @param photo - A giving object.
	 * @return the index at which the photo belongs to
	 */
	public int findIndexByPhoto(Photo photo) {
		for (int i = 0; i < photos.size(); i++)
			if (photos.get(i).equals(photo))
				return i;
		return -1;
	}

}
