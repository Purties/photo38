package model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javafx.scene.image.Image;


/**
 * Photo object class that contains its fields which include image of the photo, caption of the photo, and list of tags, it
 * also contains a calendar objcect which will use it to return the date information of a photo.
 * It implements Serializable interface so that it can't be store as binary file and loaded it into programming
 * as needed.
 * @author Xiaofa Lin, Yiwen Tao
 */
public class Photo implements Serializable 
{

	private SerializableImage image;
	private String caption;
	private List<Tag> tags;
	private Calendar cal;
	
	/**
	 * Photo constructor which initializes all its fields.
	 */
	public Photo() {
		caption = "";
		tags = new ArrayList<Tag>();
		cal = Calendar.getInstance();
		cal.set(Calendar.MILLISECOND, 0);
		image = new SerializableImage();
	}
	
	/**
	 * An alternative photo constructor given an image
	 * @param i - the image to show on screen
	 */
	public Photo(Image i) {
		this();
		image.setImage(i);
	}
	
	/**
	 * A method to add tags to a photo.
	 * @param type - tag type
	 * @param value - tag value
	 */
	public void addTag(String type, String value) {
		tags.add(new Tag(type, value));
	}

	/**
	 * A method to edit tags by find the tag by index and change its type or value respectivly.
	 * @param index	- the tag index
	 * @param type - the tag type
	 * @param value	- the tag value
	 */
	public void editTag(int index, String type, String value) {
		tags.get(index).setType(type);
		tags.get(index).setValue(value);
	}
	
	/**
	 * A method to remove tags from tag list by giving its index number.
	 * @param index	- The index of the tag
	 */
	public void removeTag(int index) {
		tags.remove(index);
	}
	
	/**
	 * A method to get Tag object by giving its index in the list.
	 * @param index	- Tag index
	 * @return Tag object
	 */
	public Tag getTag(int index) {
		return tags.get(index);
	}
	
	/**
	 * A setter to change the caption of a photo by giving the new caption.
	 * @param caption - new caption to replace the old one.
	 */
	public void setCaption(String caption) {
		this.caption = caption;
	}
	
	/**
	 * A method to get caption.
	 * @return caption String of Caption
	 */
	public String getCaption() {
		return caption;
	}
	
	/**
	 * A method to get the Calendar object.
	 * @return cal - the date got by Calendar.getInstance();
	 */

	public Calendar getCalendar() {
		return cal;
	}
	
	
	/**
	 * A method that returns a list of tag
	 * @return list of all tags
	 */
	public List<Tag> getTags() {
		return tags;
	}
	
	/**
	 * A method to get date as desired string representation.
	 * @return string of the date
	 */
	public String getDate() {
		String[] str = cal.getTime().toString().split("\\s+");
		return str[0] + " " + str[1] + " " + str[2] + ", " + str[5];
	}

	/**
	 * A method that return an image
	 * @return image of a photo
	 */
	public Image getImage() {
		return image.getImage();
	}

	/**
	 * A method to get serialized image.
	 * @return The serializable image
	 */
	public SerializableImage getSerializableImage() {
		return image;
	}
	
	/**
	 * 
	 * A method uses hash set of tags. 
	 * Checks if parameter is a subset of tags
	 * It used by search of photo by tags.
	 * @param tlist The list to be compared with
	 * @return true if tlist is a subset of tags, else false
	 */
	public boolean hasSubset(List<Tag> tlist) {
		Set<Tag> allTags = new HashSet<Tag>();
		allTags.addAll(tags);
		
		for (Tag t: tlist) {
			if (!allTags.contains(t))
				return false;
		}
		return true;
	}
	
	/**
	 * A method to check if a photo is within a specific date range.
	 * It's used by search photo by date range.
	 * @param fromDate
	 * @param toDate
	 * @return true if photo date is within range, else false
	 */
	public boolean isWithinDateRange(LocalDate fromDate, LocalDate toDate) {
		LocalDate date = cal.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		
		return date.isBefore(toDate) && date.isAfter(fromDate) || date.equals(fromDate) || date.equals(toDate);
	}
	
}
