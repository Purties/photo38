package model;

import java.io.Serializable;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;

/**
 * A class that serialize the image pixel by pixel in an two dimensional array so it could be written and read into programming
 * as needed. It contains all fileds that a image needs. Image's width and Image's height.
 *It implements Serializable interface so that it can't be store as binary file and loaded it into programming as needed.
 * @author Xiaofa Lin, Yiwen Tao
 */
public class SerializableImage implements Serializable {

	/**
	 * An image's required field width and height.
	 */
	private int width, height;
	/**
	 * Two dimensional array to store its pixels.
	 */
	private int[][] pixels;

	/**
	 * A default constructor. 
	 */
	public SerializableImage() {}

	/**
	 * A getter to get image's width.
	 * @return width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * A getter to get image's height.
	 * @return height
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * A getter to get image's pixels.
	 * @return pixels - The 2D array of pixels
	 */
	public int[][] getPixels() {
		return pixels;
	}

	/**
	 * A setter to set a image, it converts Image to 2d array of pixels
	 * @param image that would be stored.
	 */
	public void setImage(Image image) {
		width = ((int) image.getWidth());
		height = ((int) image.getHeight());
		pixels = new int[width][height];
		PixelReader r = image.getPixelReader();
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				pixels[i][j] = r.getArgb(i, j);
			}
		}
	}

	/**
	 * A getter to get image from 2D array pixels.
	 * @return Image object
	 */
	public Image getImage() {
		WritableImage image = new WritableImage(width, height);

		PixelWriter w = image.getPixelWriter();
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				w.setArgb(i, j, pixels[i][j]);
			}
		}
		return image;
	}

	/**
	 * A method to check if two images are equal by comparing each pixel
	 * @param serialImage The serializable image to be checked
	 * @return true if they're equal, else false
	 */
	public boolean equals(SerializableImage serialImage) {
		if (width != serialImage.getWidth()) {
			return false;
		}
		if (height != serialImage.getHeight()) {
			return false;
		}
		for (int i = 0; i < width; i++)
			for (int j = 0; j < height; j++)
				if (pixels[i][j] != serialImage.getPixels()[i][j])
					return false;
		return true;
	}

}
