package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * User class which contains all its fields include name of user, username of a user and password of a user, it also contains
 * a reference of list of album. It implements Serializable interface so that it can't be store as binary file and loaded it into programming
 * as needed.
 * @author Xiaofa Lin Yiwen Tao
 */
public class User implements Serializable {

	/**
	 * Fields of a user
	 */
	private String name, username, password;
	/**
	 * List of albums a user has
	 */
	private List<Album> albums;

	
	/**
	 * Users constructor which initializes all it fields.
	 * @param name		Name - User's real name
	 * @param username	username of user
	 * @param password	password of user
	 */
	public User(String name, String username, String password) {
		this.name = name;
		this.username = username;
		this.password = password;
		albums = new ArrayList<Album>();
	}
	
	/**
	 * A getter to return list of album that a user holds.
	 * @return albums
	 */
	public List<Album> getAlbums() {
		return albums;
	}
	
	/**
	 * A getter to return the name of a user.
	 * @return name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * A getter to return the username of a user.
	 */
	public String getUsername() {
		return username;
	}
	
	/**
	 * A getter to return the password of a user.
	 * @return password
	 */
	public String getPassword() {
		return password;
	}
	
	/**
	 * A getter to change the password of a user.
	 * @param password
	 */
	public void changePassword(String password) {
		this.password = password;
	}
	
	/**
	 * A method to allow a user to add an album by typing album name.
	 * @param albumName - String of Album
	 */
	public void addAlbum(String albumName) {
		albums.add(new Album(albumName));
	}
	
	/**
	 * A method of adding an existing album to album list
	 * @param a - the existing album
	 */
	public void addAlbum(Album a) 
	{
		albums.add(a);
	}
	
	/**
	 * A method that takes photo object and index of albums list add the photo to desired album.
	 * @param p - Photo added
	 * @param albumIndex - the index of the album
	 */
	public void addPhotoToAlbum(Photo p, int albumIndex) {
		albums.get(albumIndex).addPhoto(p);
	}
	
	/**
	 * A method to check if a new album exists by giving the album name.
	 * Checks to see if albumName exists already in albums.
	 * @param albumName
	 * @return true if the album name already exists, else false
	 */
	public boolean albumNameExists(String albumName) {
		for (Album a: albums)
			if (a.getName().toLowerCase().equals(albumName.trim().toLowerCase()))
				return true;
		
		return false;
	}
	
	/**
	 * A method to return an album by typing its name.
	 * @param name		The album name-String
	 * @return Given an album name, return the album
	 */
	public Album getAlbumByName(String name) {
		for(Album a : albums)
		{
			if(a.getName().equals(name)) {
				return a;
			}
		}
		return null;
	}
	
	/**
	 * A method to remove an album from list of albums.
	 * @param album
	 */
	public void removeAlbum(Album album)
	{
		albums.remove(album);
	}
	
	
	/**
	 * A method that returns the index of an album by giving the existing album object.
	 * @param a		The album being checked
	 * @return Given an album, find the index to which it resides in albums
	 */
	public int getAlbumIndexByAlbum(Album a) {
		for (int i = 0; i < albums.size(); i++)
			if (albums.get(i).getName().equals(a.getName()))
				return i;
		return -1;
	}
	
}
