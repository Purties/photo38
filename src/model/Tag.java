package model;

import java.io.Serializable;


/**
 * Tag class contains two fields which include its type and its value.
 * It implements Serializable interface so that it can't be store as binary file and loaded it into programming
 * as needed.
 * @author Xiaofa Lin, YiWen Tao
 *
 */
public class Tag implements Serializable {

	/**
	 * Type filed and value field that would be used for tag photo.
	 */
	private String type, value;
	
	/**
	 * Tag Constructor whichi initializes its fields.
	 * @param type given by users.
	 * @param value given by users.
	 */
	public Tag(String type, String value) {
		this.type = type;
		this.value = value;
	}
	
	/**
	 * A getter to get type of a tag.
	 * @return type	
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * A getter to get value of a tag.
	 * @return value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * A setter to set type of a tag by giving new type.
	 * @param type new tag type giving by users.
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	/**
	 * A setter to set value of a tag by giving new type.
	 * @param value new tag value giving by users.
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * An override method used in searching page
	 * @return true if the tag is the same
	 */
	@Override
	public boolean equals(Object obj) {
		if(obj==null || !(obj instanceof Tag)) 
		{
			   return false;
		}
		Tag t =(Tag ) obj;
        return t.getValue().equals(value) && t.getType().equals(type);
	}
	
	
	/**
	 * A override toString method that print out desired tag string representation.
	 * @return type + ": " + value
	 */
	public String toString() {
		return type + ": " + value;
	}

	/**
	 * A hash function
	 * Tags are stored in a hash set
	 */
	@Override
	public int hashCode() {
		return value.hashCode()+type.hashCode();
	}
}
