
package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * This UserList class stores User Object in an Arraylist and store them in dat file
 * This class implement the write and read method of Serializable interface so that can be used by all the classes in this
 * project. It 
 * @author Yiwen Tao, Xiaofa Lin
 */
public class UserList implements Serializable {

	/**
	 * serialVersionUID which generated automatically
	 */
	private static final long serialVersionUID = 9221355046218690511L;	
	/**
	 * direcotry
	 */
	public static final String storeDir = "dat";
	/**
	 * a file to store user information
	 */
	public static final String storeFile = "users.dat";
	/**
	 * List of users
	 */
	private List<User> users;

	/**
	 * Read the users.dat file and return the UserList
	 * @return	return the UserList model of all users
	 * @throws IOException		Exception for serialization
	 * @throws ClassNotFoundException		Exception for serialization
	 */
	public static UserList read() throws IOException, ClassNotFoundException {
		ObjectInputStream inputS = new ObjectInputStream(new FileInputStream(storeDir + File.separator + storeFile));
		UserList ulist = (UserList) inputS.readObject();
		inputS.close();
		return ulist;
	}

	/**
	 * Write Given Userlist into users.dat
	 * @param ulist	The UserList model to write with
	 * @throws IOException		Exception for serialization
	 */
	public static void write (UserList ulist) throws IOException {
		ObjectOutputStream OutputS = new ObjectOutputStream(new FileOutputStream(storeDir + File.separator + storeFile));
		OutputS.writeObject(ulist);
		OutputS.close();
	}

	/**
	 * UserList constructor which initializes the array list
	 */
	public UserList() {
		users = new ArrayList<User>();
	}


	/** 
	 * A getter returns list of users
	 * @return List of User
	 */
	public List<User> getUserList()
	{
		return users;
	}

	/**
	 * A method to return proper representation of user information.
	 * @return String - all usernames
	 */
	public String toString() {
		if (users == null)
			return "N/A";
		String output = "";
		for(User u : users)
		{
			output += u.getUsername() + " ";
		}
		return output;
	}

	/**
	 * A method that add a user to user list.
	 * @param u - The user is going to be added	    
	 */  
	public void addUserToList(User u)
	{
		users.add(u);
	}

	/**
	 * A method that delete a user from user list.
	 * @param u - The user is going to be removed    
	 */
	public void removeUserFromList(User u)
	{
		users.remove(u);
	}

	/**
	 * A method check if a new user which is going to added is alreay in the list.
	 * @param un - the username given 
	 * @param p - the password given
	 * @return boolean - return true if username and password is correct and also in the list  
	 */
	public boolean isUserInList(String un, String p)
	{
		for(User u : users)
		{
			if (u.getUsername().equals(un) && u.getPassword().equals(p)) 
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * A method to checks if a specific username already exists
	 * @param un - the username that is going to be checked
	 * @return boolean - return true if username is in the list 
	 */
	public boolean userExists(String un)
	{
		for(User u : users)
		{
			if (u.getUsername().equals(un))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 *A method to get user object by giving username field.
	 * @param username	the user's username (unique)
	 * @return user with the same username
	 */
	public User getUserByUsername(String username) {
		for (User u : users)
		{
			if (u.getUsername().equals(username))
				return u;
		}
		return null;
	}
}
