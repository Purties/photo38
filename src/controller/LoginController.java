package controller;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.*;	

/**
 * Login Controller
 * If user is admin, lead to AdminPage
 * If user is normal user, lead to UserAlbumPage
 * @author Yiwen Tao, Xiaofa Lin
 */

public class LoginController {

	@FXML private Button loginButton;
	@FXML private TextField usernameField;
	@FXML private PasswordField passwordField;
	@FXML private Text actionTarget;

	/**
	 * Login Bottom
	 */

	@FXML protected void loginBttn(ActionEvent event) throws ClassNotFoundException {

		String username = usernameField.getText();
		String password = passwordField.getText();
		Parent parent;

		UserList ulist = null;
		try {
			ulist = UserList.read();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		try {
			// If the user is admin
			if(username.equals("admin") && password.equals("admin")){	 
				FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/AdminHome.fxml"));
				parent = (Parent) loader.load();

				AdminController ctrl = loader.getController();
				Scene scene = new Scene(parent);

				Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();	

				ctrl.start(app_stage);

				app_stage.setScene(scene);
				app_stage.show();  
			}
			// If the user is normal user
			if(ulist.isUserInList(username, password)) { 
				FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/UserHome.fxml"));
				parent = (Parent) loader.load();
				UserHomeController ctrl = loader.<UserHomeController>getController();

				ctrl.setUlist(ulist);
				ctrl.setUser(ulist.getUserByUsername(username));
				Scene scene = new Scene(parent);

				Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();	

				ctrl.start(app_stage);

				app_stage.setScene(scene);
				app_stage.show();  
			}
			else
				actionTarget.setText("Incorrect username and password."); 
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}