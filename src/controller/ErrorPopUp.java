package controller;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/**
 * Errorable Interface for showing errors in an alert
 * @author Yiwen Tao, Xiaofa Lin
 */
public interface ErrorPopUp {
	
	/**
	 * errorDisplay
	 * Pops up the error dialog
	 */

	default void errorDisplay(String error) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Error");
		alert.setHeaderText("Error");
		String content = error;
		alert.setContentText(content);
		alert.showAndWait();
	}

}
