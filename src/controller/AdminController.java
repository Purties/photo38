package controller;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Callback;
import model.*;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleStringProperty;

/**
 * Admin Controller Page
 * It can add and delete different users
 * @author Yiwen Tao, Xiaofa Lin
 */

public class AdminController implements ErrorPopUp{

	@FXML
	TableView<User> table;

	@FXML
	TableColumn<User,String> usernameColumn;

	@FXML
	TableColumn<User,String> passwordColumn;

	@FXML
	TableColumn<User,String> nameColumn;

	@FXML
	TableColumn<User,User> deleteColumn;

	private ObservableList<User> obsList;
	private List<User> users = new ArrayList<User>();
	private UserList ulist;

	public void start(Stage mainStage) throws ClassNotFoundException, IOException {
		
		ulist = UserList.read();
		users = ulist.getUserList();
		// We read userlist from dat file and create an observable ArrayList
		obsList = FXCollections.observableArrayList(users);

		usernameColumn.setCellValueFactory(new Callback<CellDataFeatures<User, String>, ObservableValue<String>>() {
			public ObservableValue<String> call(CellDataFeatures<User, String> u) {
				return new SimpleStringProperty(u.getValue().getUsername());
			}
		});


		nameColumn.setCellValueFactory(new Callback<CellDataFeatures<User, String>, ObservableValue<String>>() {
			public ObservableValue<String> call(CellDataFeatures<User, String> u) {
				return new SimpleStringProperty(u.getValue().getName());
			}
		});

		passwordColumn.setCellValueFactory(new Callback<CellDataFeatures<User, String>, ObservableValue<String>>() {
			public ObservableValue<String> call(CellDataFeatures<User, String> u) {
				return new SimpleStringProperty(u.getValue().getPassword());
			}
		});

		deleteColumn.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
		deleteColumn.setCellFactory(param -> new TableCell<User,User>() {
			private final Button deleteButton = new Button("Delete");

			@Override
			protected void updateItem(User user, boolean empty) {
				super.updateItem(user, empty);

				if (user == null) {
					setGraphic(null);
					return;
				}
				setGraphic(deleteButton);
				deleteButton.setOnAction(event -> {Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Delete User");
					alert.setHeaderText("Delete");
					alert.setContentText("Are you sure you want to delete " + user.getUsername() + "?");
					Optional<ButtonType> result = alert.showAndWait();
					if(!user.getName().equals("admin") && result.isPresent())
					{
						obsList.remove(user);
						users.remove(user);
						try {
							UserList.write(ulist);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					else if(user.getName().equals("admin"))
						errorDisplay("You cannot delete the admin!");});
			}
		});
		table.setItems(obsList);		
		if (!obsList.isEmpty()) {
			table.getSelectionModel().select(0);
		}
	}

	/**
	 * handleAddButton
	 * Add an user to userlist
	 */

	@FXML
	private void AddBttn(ActionEvent event) throws IOException {
		int index = table.getSelectionModel().getSelectedIndex();
		Dialog<User> dialog = new Dialog<>();
		dialog.setTitle("User");
		dialog.setHeaderText("Create a New User");
		dialog.setResizable(false);

		Label usernameLabel = new Label("Username: ");
		Label passwordLabel = new Label("Password: ");
		Label nameLabel = new Label("User's Name: ");
		TextField usernameTextField = new TextField();
		TextField passwordTextField = new TextField();
		TextField nameTextField = new TextField();

		GridPane grid = new GridPane();
		grid.add(usernameLabel, 1, 2);
		grid.add(usernameTextField, 2, 2);
		grid.add(passwordLabel, 1, 3);
		grid.add(passwordTextField, 2, 3);
		grid.add(nameLabel, 1, 1);
		grid.add(nameTextField, 2, 1);

		dialog.getDialogPane().setContent(grid);
		ButtonType buttonTypeOk = new ButtonType("Add", ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().add(buttonTypeOk);
		dialog.setResultConverter(new Callback<ButtonType, User>() {
			@Override
			public User call(ButtonType b) {
				if (b == buttonTypeOk) {

					String error = checkFields(usernameTextField.getText(),passwordTextField.getText(),nameTextField.getText());

					if (error != null) {
						errorDisplay(error);
						return null;
					}

					return new User(nameTextField.getText().trim(),usernameTextField.getText().trim(),
							passwordTextField.getText().trim());
				}
				return null;
			}
		});
		Optional<User> result = dialog.showAndWait();

		if (result.isPresent()) {
			User tempUser = (User) result.get();
			obsList.add(tempUser);
			users.add(tempUser);
			UserList.write(ulist);
			if (obsList.size() == 1) {
				table.getSelectionModel().select(0);
			}
			else
			{
				index = 0;
				for(User s: ulist.getUserList())
				{
					if(s == tempUser)
					{
						table.getSelectionModel().select(index);
						break;
					}
					index++;
				}
			}

		}
	}
	/**
	 * Check input fields if admin has anything missing
	 * @return Error Message
	 */
	
	private String checkFields(String username, String password, String name) {
		if (username.trim().isEmpty()) {
			return "Username is a required field.";
		}
		else if (password.trim().isEmpty()) {
			return "Password is a required field.";
		}
		else if (name.trim().isEmpty()) {
			return "Full Name is a required field.";
		}
		if (ulist.userExists(username)) {
			return "This username is already taken, please try another username.";
		} else {
			return null;
		}
	}
	

	/**
	 * handleLogoutButton
	 * Logout
	 */

	@FXML 
	protected void LogoutBttn(ActionEvent event) throws ClassNotFoundException {
    	Parent parent;
		try {	
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/LoginPage.fxml"));
			parent = (Parent) loader.load();
			Scene scene = new Scene(parent);		
			Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();			             
			app_stage.setScene(scene);
			app_stage.show();  
		} catch (IOException e) {
			e.printStackTrace();
		}    
	}
}
